from rest_framework import serializers
from .models import Spots, SkyEvents

class SpotSerializer(serializers.ModelSerializer):
    class Meta:
        model = Spots
        fields = ['nom', 'longitude', 'latitude', 'description', 'photo']

class SkyEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = SkyEvents
        fields = ['nom', 'date', 'description']
# class SpotSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Spot
#         fields = ['nom', 'longitude', 'latitude', 'description', 'photo']