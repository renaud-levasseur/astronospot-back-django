from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('spots', views.SpotList.as_view(), name='spot-list'),
    path('spots/<int:pk>', views.SpotDetail.as_view(), name='spot-detail'),
    path('sky-events', views.SkyEventsList.as_view(), name='sky-events-list')
]