from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework import generics
from .models import Spots, SkyEvents
from .serializer import SpotSerializer, SkyEventSerializer

def index(request):
    return JsonResponse(
    {
        'simple': 'simple'
    })

class SpotList(generics.ListAPIView):
    queryset = Spots.objects.all()
    serializer_class = SpotSerializer

class SpotDetail(generics.RetrieveAPIView):
    queryset = Spots.objects.all()
    serializer_class = SpotSerializer

class SkyEventsList(generics.ListAPIView):
    queryset = SkyEvents.objects.all()
    serializer_class = SkyEventSerializer