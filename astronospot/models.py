from django.db import models

class Spots (models.Model):
    nom = models.CharField(max_length=200)
    longitude = models.CharField(max_length=200)
    latitude = models.CharField(max_length=200)
    description = models.CharField(max_length=200, blank=False, null=False)
    photo = models.CharField(max_length=200, blank=False, null=False)

class SkyEvents (models.Model):
    nom = models.CharField(max_length=200)
    date = models.CharField(max_length=200)
    description = models.CharField(max_length=200, blank=False, null=False)