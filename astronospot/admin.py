from django.contrib import admin
from astronospot.models import Spots, SkyEvents

admin.site.register(Spots)
admin.site.register(SkyEvents)