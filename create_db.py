import sqlite3
try:
    conn = sqlite3.connect('db.sqlite3')
    sql = ['''CREATE TABLE users (
            id INTEGER PRIMARY KEY,
            fullname text not Null,
            email text unique not Null,
            password text not Null
            );
            ''',
            '''CREATE TABLE spots (
            id INTEGER PRIMARY KEY,
            id_user text,
            nom text not Null,
            longitude text not Null,
            latitude text not Null,
            description text,
            photo text
            );''',
            '''CREATE TABLE pictures (
            id INTEGER PRIMARY KEY,
            id_spot text,
            url text
            );''',
            '''CREATE TABLE comments (
            id INTEGER PRIMARY KEY,
            id_spot text,
            id_user text,
            description text
            );''',
            '''CREATE TABLE evaluation (
            id INTEGER PRIMARY KEY,
            id_spot text,
            id_user text,
            value text
            );''',
            '''CREATE TABLE frequentation (
            id INTEGER PRIMARY KEY,
            id_spot text,
            id_user text,
            value text
            );''',
            '''CREATE TABLE sky_events (
            id INTEGER PRIMARY KEY,
            nom text,
            description text,
            date text
            );''',
            '''CREATE TABLE tools (
            id INTEGER PRIMARY KEY,
            id_spot text,
            description text,
            quantity text
            )
    ;''']
    cur = conn.cursor()
    print("Connexion réussie à SQLite")
    for element in sql:
        cur.execute(element)
    conn.commit()
    print("Table SQLite est créée")
    cur.close()
    conn.close()
    print("Connexion SQLite est fermée")
except sqlite3.Error as error:
    print("Erreur lors de la création du table SQLite", error)